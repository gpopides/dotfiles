set relativenumber number
set splitbelow
set splitright
set clipboard=unnamedplus
set hidden
set mouse=a
syntax on
set ignorecase
set previewheight=20
set hlsearch
set cmdheight=0

call plug#begin('~/.vim/plugged') 
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'elixir-editors/vim-elixir'
Plug 'nvim-lualine/lualine.nvim'
Plug 'honza/vim-snippets'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-fugitive'
Plug 'justinmk/vim-dirvish'
Plug 'kristijanhusak/vim-dirvish-git'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'neovim/nvim-lspconfig'
Plug 'ibhagwan/fzf-lua', {'branch': 'main'}
Plug 'windwp/nvim-autopairs'
Plug 'ggandor/leap.nvim'
Plug 'kylechui/nvim-surround'
Plug 'liuchengxu/vista.vim'
Plug 'vim-test/vim-test'
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-buffer'
Plug 'dcampos/nvim-snippy'
Plug 'dcampos/cmp-snippy'
Plug 'nvim-orgmode/orgmode'
Plug 'akinsho/org-bullets.nvim'
Plug 'shaunsingh/nord.nvim'


call plug#end()

colorscheme nord

vnoremap <C-c> "*Y :let @+=@*<CR>
map <C-v> "+P

"bindings
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
noremap ,g :G<CR>
noremap <leader>s :Vista<CR>
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

"fzf
noremap ,f :FzfLua git_files<CR>
noremap <leader>b :FzfLua buffers<CR>
nnoremap <leader>gf :FzfLua grep<CR>
nnoremap <leader>gw :FzfLua grep_cword<CR>

nnoremap gr :FzfLua lsp_references<CR>
nnoremap gd :FzfLua lsp_definitions<CR>
nnoremap gi :FzfLua lsp_implementations<CR>

noremap <leader>d :Dirvish <CR>
noremap ,v :vsplit<CR>
nnoremap <silent> K :call ShowDocumentation()<CR>

let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" Bring search results to midscreen
nnoremap n nzz
nnoremap N Nzz

"disable hex
nnoremap Q <nop>

noremap <C-L>  :nohls<CR><C-L>
noremap <leader>gl  :Git<CR><C-L>
noremap <leader>gc  :Git commit<CR><C-L>

nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>l :TestLast<CR>
let test#strategy = "neovim"

"let g:coq_settings = { 'auto_start': v:true }

set completeopt=menu,menuone,noselect

lua << EOF

--require("coq")

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', vim.lsp.buf.formatting, bufopts)
end

require'nvim-treesitter.configs'.setup {
	-- A list of parser names, or "all"
	ensure_installed = { "elixir", "heex", "erlang", "eex", "org"},

	-- Install parsers synchronously (only applied to `ensure_installed`)
	sync_install = false,

	-- Automatically install missing parsers when entering buffer
	auto_install = true,

	highlight = {
		-- `false` will disable the whole extension
		enable = true,

		additional_vim_regex_highlighting = false,
    additional_vim_regex_highlighting = {'org'}, 
	},
}



require('lualine').setup{
  options = { theme = 'nord' }
}
require("nvim-autopairs").setup {}

require("nvim-surround").setup {}

require('leap').set_default_keymaps()

vim.g.symbols_outline = {
    highlight_hovered_item = true,
    show_guides = false,
    auto_preview = false,
    position = 'right',
    relative_width = true,
    width = 25,
    auto_close = false,
    show_numbers = false,
    show_relative_numbers = false,
    show_symbol_details = true,
    preview_bg_highlight = 'Pmenu',
    keymaps = { -- These keymaps can be a string or a table for multiple keys
        close = {"<Esc>", "q"},
        goto_location = "<Cr>",
        focus_location = "o",
        hover_symbol = "<C-space>",
        toggle_preview = "K",
        rename_symbol = "r",
        code_actions = "a",
    },
    lsp_blacklist = {},
    symbol_blacklist = {},
}

require('fzf-lua').setup{
	winopts = { height=0.55, width=0.55 }
}

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local cmp = require'cmp'
cmp.setup({
	snippet = {
		expand = function(args)
			require('snippy').expand_snippet(args.body) -- For `snippy` users.
		end,
	},
	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},
	mapping = cmp.mapping.preset.insert({
		['<C-b>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
      end
    end, {"i", "s"}),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'snippy' },
		{ name = 'orgmode' },
	}, {
		{ name = 'buffer' },
	})
})

require('snippy').setup({
    mappings = {
        is = {
            ['<C-j>'] = 'expand_or_advance',
            ['<C-k>'] = 'previous',
        },
        nx = {
            ['<leader>x'] = 'cut_text',
        },
    },
})

local nvim_lsp = require'lspconfig'

nvim_lsp.elixirls.setup({
-- Unix
  cmd = { "/home/woops/.elixir-ls/release/language_server.sh" };
	on_attach = on_attach,
})

local opts = {
    tools = { 
        autoSetHints = true,
        inlay_hints = {
            show_parameter_hints = false,
            parameter_hints_prefix = "",
            other_hints_prefix = "",
        },
    },

    server = {
        on_attach = on_attach,
        settings = {
            ["rust-analyzer"] = {
                checkOnSave = {
                    command = "clippy"
                },
            }
        }
    },
}

require('orgmode').setup_ts_grammar()

require('orgmode').setup({
  org_agenda_files = {'~/Nextcloud/org/*', '~/my_orgs/**/*'},
  org_default_notes_file = '~/Nextcloud/org/refile.org',
})
require('org-bullets').setup()

EOF
