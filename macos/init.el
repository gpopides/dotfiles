;; package --- Summary
(menu-bar-mode -1)
(scroll-bar-mode -1) 
(tool-bar-mode -1) 
(setq evil-want-keybinding nil)

(require 'package)
(require 'general)
(require 'tree-sitter)
(require 'evil)
(when (require 'evil-collection nil t)
  (evil-collection-init))
(require 'smartparens-config)
;;; Code:
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
;;(package-refresh-contents)

(use-package lsp-ui :ensure t)
(use-package flycheck :ensure t
  :config
  (global-flycheck-mode))
(use-package fzf :ensure t)
(use-package company :ensure t)
(use-package general :ensure t)
(use-package elixir-mode :ensure t)
(use-package magit :ensure t)
(use-package mood-line :ensure t)
(use-package evil-collection :ensure t)
(use-package tree-sitter :ensure t)
(use-package tree-sitter-langs :ensure t)
(use-package deadgrep :ensure t)
(use-package format-all
  :preface
  :config
  (global-set-key (kbd "M-F") 'format-all-buffer))

(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  )
(use-package elixir-mode :ensure t)
;; (use-package  elixir-mode :ensure t)
(use-package eldoc-box
  :ensure t
  :init
  (eldoc-box-hover-mode)
  )
(use-package smartparens :ensure t)
(use-package evil-nerd-commenter :ensure t)
(use-package better-jumper)
(use-package dirvish
  :ensure t
  :config
  ;; let Dirvish takes over Dired globally
  (dirvish-override-dired-mode))
(use-package phpactor :ensure t)
(use-package company-phpactor :ensure t)
(use-package company-box :ensure t)
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))
(use-package company-posframe :ensure t)

;; gruvbox
(load-theme 'gruvbox-dark-hard t)
(mood-line-mode)
(yas-global-mode)
;;
;; lsp configuration
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; general config
(better-jumper-mode +1)
(setq-default display-line-numbers 'relative)
(menu-bar-mode -1)

(global-unset-key (kbd "K"))
(general-def evil-motion-state-map
  "K" 'lsp-ui-doc-show
  )

(evil-mode 1)

(setq evil-want-minibuffer t)
(add-hook 'after-init-hook 'global-company-mode)


;; general
(yas-global-mode)
(general-create-definer my-leader-def :prefix ";")

(my-leader-def
  :keymaps 'normal
  "f" 'fzf-git
  "v" 'evil-window-vsplit
  "d" 'dirvish
  "g" 'magit
  "b" 'ibuffer
  "t" 'lsp-ui-imenu
  "r" 'lsp-rename
  )

(general-define-key
 :states 'normal
 :prefix "SPC"
 "c" `(lambda ()
	(interactive)
	(find-file "~/.emacs.d/init.el")))

;; keybindinds
(global-set-key (kbd "C-x C-g") #'deadgrep)
(global-set-key (kbd "C-x c") 'evilnc-comment-or-uncomment-lines)
(global-set-key (kbd "C-x r") 'lsp-find-references)

(setq completion-styles '(flex substring))

(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)

(setq scroll-step 1)
(setq scroll-conservatively 10000)
(setq auto-window-vscroll nil)

;; Go - lsp-mode
;; (setq lsp-ui-doc-enable t)
;; (setq lsp-eldoc-hook nil)
;; (setq lsp-ui-doc-delay 2)
;; Set up before-save hooks to format buffer and add/delete imports.

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)

(marginalia-mode)
(company-mode)

(exec-path-from-shell-initialize)

(setq lsp-signature-auto-activate t lsp-signature-doc-lines 1)
(setq lsp-ui-doc-enable 1)
(setq lsp-ui-sideline-enable nil)
(setq global-whitespace-mode nil)
(set-frame-font "Jetbrains Mono:size=12")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("6b5c518d1c250a8ce17463b7e435e9e20faa84f3f7defba8b579d4f5925f60c1" default))
 '(global-whitespace-mode nil)
 '(lsp-ui-doc-position 'bottom)
 '(lsp-ui-doc-show-with-cursor nil)
 '(package-selected-packages
   '(company-posframe company-box ivy-posframe flycheck-posframe company-php evil-surround flycheck flymake-go lsp-ivy which-key-posframe which-key eldoc-go vterm @ php-mode company-phpactor phpactor exec-path-from-shell marginalia lsp-mode lsp-ui better-jumper yasnippet-snippets yasnippet format-all fzf eldoc-box eldoc company-mode smartparens tree-sitter-langs tree-sitter evil-collection dirvish mood-line magit elixir-mode general use-package gruvbox-theme fzf evil-visual-mark-mode counsel company)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
