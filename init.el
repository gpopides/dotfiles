;;; package --- Summary
(require 'package)
(require 'general)
(require 'tree-sitter)
(require 'evil)
(require 'smartparens-config)

;;; Code:
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
;; (package-refresh-contents)

;; packages
;;(use-package company-mode :ensure t)
  (use-package lsp-mode
    :commands lsp
    :ensure t
    :diminish lsp-mode
    :hook
    (elixir-mode . lsp)
    :init
    (add-to-list 'exec-path "~/.elixir-ls/release"))
(use-package lsp-ui :ensure t)
(use-package flycheck :ensure t
  :config
  (global-flycheck-mode))
(use-package fzf :ensure t)
(use-package general :ensure t)
(use-package elixir-mode :ensure t)
(use-package magit :ensure t)
(use-package mood-line :ensure t)
(use-package evil-collection :ensure t)
(use-package tree-sitter :ensure t)
(use-package frog-jump-buffer :ensure t)
(use-package deadgrep :ensure t)
(use-package frog-jump-buffer :ensure t)
(use-package format-all
  :preface
  :config
  (global-set-key (kbd "M-F") 'format-all-buffer))

(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  )
(use-package yasnippet :ensure t)
(use-package elixir-mode :ensure t)
(use-package eldoc-box
  :ensure t
  :init
  (eldoc-box-hover-mode)
 )

;; gruvbox
(load-theme 'gruvbox-dark-hard t)
(mood-line-mode)
(yas-global-mode)

;; lsp configuration
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; general config

;; disable automatic scrolling to make navigation work like vim
(setq recenter-redisplay nil)
(setq scroll-conservatively 101)
(setq scroll-margin 0)

(global-set-key (kbd "C-x C-b") 'frog-jump-buffer)
(setq-default display-line-numbers 'relative)
(evil-collection-init)
(menu-bar-mode -1)
(setq select-enable-clipboard nil)

(evil-mode 1)
(add-hook 'after-init-hook 'global-company-mode)
(evilnc-default-hotkeys t)

;; general (package)
(general-create-definer my-leader-def :prefix ";")

(my-leader-def
  :keymaps 'normal
  "f" 'fzf-find-file
  "v" 'evil-window-vsplit
  "d" 'dirvish
  "g" 'magit
  )

(general-define-key
 :states 'normal
 :prefix "SPC"
 "c" `(lambda ()
	(interactive)
	(find-file "~/.emacs.d/init.el")))

;; grep
(global-set-key (kbd "C-x C-g") #'deadgrep)

(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)

(setq scroll-step 1)
(setq scroll-conservatively 10000)
(setq auto-window-vscroll nil)

;; Go - lsp-mode
;; Set up before-save hooks to format buffer and add/delete imports.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("6b5c518d1c250a8ce17463b7e435e9e20faa84f3f7defba8b579d4f5925f60c1" default))
 '(package-selected-packages
   '(evil-nerd-commenter flycheck lsp-mode fzf eldoc-box eldoc yasnippet-snippets yasnippet company-mode smartparens frog-jump-buffer ivy-posframe tree-sitter-langs tree-sitter evil-collection dirvish mood-line magit elixir-mode general use-package lsp-ui gruvbox-theme fzf evil-visual-mark-mode counsel company)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
